package com.enderage.core.meta;

import org.bukkit.entity.Entity;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import com.enderage.core.Core;

/**
 * 
 * @author TwistPvP
 * 
 */
public class EntityMeta {

	public static String getExtraMeta(Entity e, String key) {
		try {
			MetadataValue fmv = e.getMetadata(key).get(0);
			if (fmv != null) {
				return fmv.asString();
			}
		} catch (Exception ex) {
		}
		return null;
	}

	public static void setExtraMeta(Entity e, String key, String value) {
		e.setMetadata(key, new FixedMetadataValue(Core.get(), value));
	}
}
