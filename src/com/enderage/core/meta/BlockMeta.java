package com.enderage.core.meta;

import java.util.HashMap;
import java.util.Map;
import org.bukkit.Location;

/**
 * 
 * @author TwistPvP
 * 
 */
public class BlockMeta {

	private static Map<Location, Map<String, String>> metadata = new HashMap<Location, Map<String, String>>();

	public static void put(Location loc, String key, String value) {
		if (!metadata.containsKey(loc)) {
			metadata.put(loc, new HashMap<String, String>());
		}
		metadata.get(loc).put(key, value);
	}

	public static String get(Location loc, String key) {
		if (!metadata.containsKey(loc)) {
			metadata.put(loc, new HashMap<String, String>());
		}
		return metadata.get(loc).get(key);
	}

	public static Map<String, String> getMetadata(Location loc) {
		if (!metadata.containsKey(loc)) {
			metadata.put(loc, new HashMap<String, String>());
		}
		return metadata.get(loc);
	}

	public static HashMap<Location, Map<String, String>> getAllMetadata() {
		return new HashMap<>(metadata);
	}

	public static int getInt(Location loc, String key) {
		if (!metadata.containsKey(loc)) {
			metadata.put(loc, new HashMap<String, String>());
		}
		return Integer.parseInt(metadata.get(loc).get(key));
	}

	public static double getDouble(Location loc, String key) {
		if (!metadata.containsKey(loc)) {
			metadata.put(loc, new HashMap<String, String>());
		}
		return Double.parseDouble(metadata.get(loc).get(key));
	}

	public static boolean getBool(Location loc, String key) {
		if (!metadata.containsKey(loc)) {
			metadata.put(loc, new HashMap<String, String>());
		}
		return Boolean.parseBoolean(metadata.get(loc).get(key));
	}
}
