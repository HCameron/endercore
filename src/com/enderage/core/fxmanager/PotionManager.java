package com.enderage.core.fxmanager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.enderage.core.Core;

/**
 * 
 * @author TwistPvP
 * 
 */
public class PotionManager {

	private HashMap<String, ArrayList<EnderPotionEffect>> effects = new HashMap<>();
	private HashMap<String, ArrayList<EnderPotionEffect>> addeffects = new HashMap<>();

	public PotionManager() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(Core.get(),
				new Runnable() {
					@Override
					public void run() {
						tick();
					}
				}, 0L, 1L);
	}

	public void addEffect(Player p, EnderPotionEffect spe) {
		if (!addeffects.containsKey(p.getUniqueId().toString())) {
			addeffects.put(p.getUniqueId().toString(),
					new ArrayList<EnderPotionEffect>());
		}
		addeffects.get(p.getUniqueId().toString()).add(spe);
	}

	// generics to fix brackets again..

	@SuppressWarnings("unchecked")
	public EnderPotionEffect getSPE(int id) {
		for (ArrayList<EnderPotionEffect> list : ((HashMap<String, ArrayList<EnderPotionEffect>>) effects
				.clone()).values()) {
			for (EnderPotionEffect spe : list) {
				if (spe.getID() == id) {
					return spe;
				}
			}
		}
		return null;
	}

	public EnderPotionEffect[] getPlayerSPES(Player p) {
		if (effects.get(p.getUniqueId().toString()) != null) {
			return effects.get(p.getUniqueId().toString()).toArray(
					new EnderPotionEffect[effects.get(
							p.getUniqueId().toString()).size()]);
		}
		return new EnderPotionEffect[0];
	}

	@SuppressWarnings("unchecked")
	public EnderPotionEffect[] getTypeSPES(String IDTYPE) {
		ArrayList<EnderPotionEffect> appliesto = new ArrayList<>();
		for (ArrayList<EnderPotionEffect> list : ((HashMap<String, ArrayList<EnderPotionEffect>>) effects
				.clone()).values()) {
			for (EnderPotionEffect spe : list) {
				if (spe.getIDTYPE().equals(IDTYPE)) {
					appliesto.add(spe);
				}
			}
		}
		return appliesto.toArray(new EnderPotionEffect[appliesto.size()]);
	}

	public EnderPotionEffect[] getPlayerTypeSPES(Player p, String IDTYPE) {
		if (effects.get(p.getUniqueId().toString()) != null) {
			ArrayList<EnderPotionEffect> appliesto = new ArrayList<>();
			for (EnderPotionEffect spe : effects
					.get(p.getUniqueId().toString())) {
				if (spe.getIDTYPE().equals(IDTYPE)) {
					appliesto.add(spe);
				}
			}
			return appliesto.toArray(new EnderPotionEffect[appliesto.size()]);
		}
		return new EnderPotionEffect[0];
	}

	public Player getPlayerFromSPE(EnderPotionEffect spe) {
		return getPlayerFromSPE(spe.getID());
	}

	@SuppressWarnings("unchecked")
	public Player getPlayerFromSPE(int id) {
		for (String puuid : ((HashMap<String, ArrayList<EnderPotionEffect>>) effects
				.clone()).keySet()) {
			for (EnderPotionEffect spe : effects.get(puuid)) {
				if (spe.getID() == id) {
					return Bukkit.getPlayer(UUID.fromString(puuid));
				}
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public void tick() {
		HashMap<String, ArrayList<EnderPotionEffect>> EffectsCopy = (HashMap<String, ArrayList<EnderPotionEffect>>) effects
				.clone();
		HashMap<String, ArrayList<EnderPotionEffect>> newEffects = new HashMap<>();
		for (String puuid : EffectsCopy.keySet()) {
			Player p = Bukkit.getPlayer(UUID.fromString(puuid));
			if (p == null) {
				continue;
			}
			if (!newEffects.containsKey(puuid.toString())) {
				newEffects.put(puuid, new ArrayList<EnderPotionEffect>());
			}
			for (EnderPotionEffect spe : effects.get(puuid)) {
				spe.setRemainingTime(spe.getRemainingTime() - 1);
				if (spe.getRemainingTime() > 0) {
					newEffects.get(puuid).add(spe);
				}
			}
		}
		HashMap<String, ArrayList<EnderPotionEffect>> AddEffectsCopy = (HashMap<String, ArrayList<EnderPotionEffect>>) addeffects
				.clone();
		addeffects.clear();
		for (String puuid : AddEffectsCopy.keySet()) {
			if (!newEffects.containsKey(puuid)) {
				newEffects.put(puuid, new ArrayList<EnderPotionEffect>());
			}
			for (EnderPotionEffect spe : AddEffectsCopy.get(puuid)) {
				newEffects.get(puuid).add(spe);
			}
		}
		effects = newEffects;
		applyEffects();
	}

	@SuppressWarnings("unchecked")
	public void applyEffects() {
		for (String puuid : ((HashMap<String, ArrayList<EnderPotionEffect>>) effects
				.clone()).keySet()) {
			Player p = Bukkit.getPlayer(UUID.fromString(puuid));
			if (p == null) {
				continue;
			}
			HashMap<PotionEffectType, Integer[]> applyEffects = new HashMap<>();
			for (EnderPotionEffect spe : effects.get(puuid)) {
				if (applyEffects.containsKey(spe.getPet())) {
					if (spe.getPriority() >= applyEffects.get(spe.getPet())[0]) {
						applyEffects.put(
								spe.getPet(),
								new Integer[] { spe.getPriority(),
										spe.getAmplifier() });
					}
				} else {
					applyEffects.put(
							spe.getPet(),
							new Integer[] { spe.getPriority(),
									spe.getAmplifier() });
				}
			}
			for (PotionEffect pe : p.getActivePotionEffects()) {
				p.removePotionEffect(pe.getType());
			}
			for (PotionEffectType pet : applyEffects.keySet()) {
				p.addPotionEffect(new PotionEffect(pet, Integer.MAX_VALUE,
						applyEffects.get(pet)[1]));
			}
		}
	}
}
