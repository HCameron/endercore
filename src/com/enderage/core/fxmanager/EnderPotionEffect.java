package com.enderage.core.fxmanager;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.potion.PotionEffectType;

import com.enderage.core.Core;

/**
 * 
 * @author TwistPvP
 */
public class EnderPotionEffect {

	private PotionEffectType pet;
	private long remainingTime;
	private int priority;
	private int amplifier;
	private int id;
	private String IDTYPE;

	public EnderPotionEffect(PotionEffectType pet, long remainingTime,
			int priority, int amplifier) {
		this.pet = pet;
		this.remainingTime = remainingTime;
		this.priority = priority;
		this.amplifier = amplifier;
		int newid = (new Random()).nextInt(Integer.MAX_VALUE);
		while (Core.getPotionManager().getSPE(newid) != null) {
			newid = (new Random()).nextInt(Integer.MAX_VALUE);
		}
		this.id = newid;
		IDTYPE = "";
	}

	public String getIDTYPE() {
		return IDTYPE;
	}

	public void setIDTYPE(String IDTYPE) {
		this.IDTYPE = IDTYPE;
	}

	public PotionEffectType getPet() {
		return pet;
	}

	public int getPriority() {
		return priority;
	}

	public int getAmplifier() {
		return amplifier;
	}

	public int getId() {
		return id;
	}

	public long getRemainingTime() {
		return remainingTime;
	}

	public void setRemainingTime(long remainingTime) {
		this.remainingTime = remainingTime;
	}

	public void cancel() {
		this.remainingTime = 0;
	}

	public int getID() {
		return id;
	}

	public String serialize() {
		String serialized = pet.getName() + "," + remainingTime + ","
				+ priority + "," + amplifier + "," + id + "," + IDTYPE;
		return serialized;
	}

	public static String serializeArray(EnderPotionEffect[] spes) {
		String serialized = "";
		if (spes != null) {
			for (EnderPotionEffect spe : spes) {
				serialized += spe.serialize() + "!";
			}
		}
		if (serialized.length() > 0) {
			serialized = serialized.substring(0, serialized.length() - 1);
		}
		return serialized;
	}

	public static EnderPotionEffect deserialize(String serialized) {
		PotionEffectType pet = PotionEffectType
				.getByName(serialized.split(",")[0]);
		long remainingTime = Long.parseLong(serialized.split(",")[1]);
		int priority = Integer.parseInt(serialized.split(",")[2]);
		int amplifier = Integer.parseInt(serialized.split(",")[3]);
		@SuppressWarnings("unused")
		int id = Integer.parseInt(serialized.split(",")[4]);
		String IDTYPE = serialized.split(",")[5];
		EnderPotionEffect spe = new EnderPotionEffect(pet, remainingTime,
				priority, amplifier);
		spe.setIDTYPE(IDTYPE);
		return spe;
	}

	public static EnderPotionEffect[] deserializeArray(String serialized) {
		ArrayList<EnderPotionEffect> effects = new ArrayList<>();
		for (String s : serialized.split("!")) {
			if (s.length() > 0) {
				effects.add(deserialize(s));
			}
		}
		return effects.toArray(new EnderPotionEffect[effects.size()]);
	}
}
