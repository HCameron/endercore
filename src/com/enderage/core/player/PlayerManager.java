package com.enderage.core.player;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.enderage.core.Core;
import com.enderage.core.callbacks.IPlayerLoaded;
import com.enderage.core.callbacks.ISparkPlayerFound;
import com.enderage.core.database.Collection;
import com.enderage.core.database.instance.Setting;
import com.enderage.core.scheduler.callbacks.ExceptionJobCallback;
import com.enderage.core.scheduler.callbacks.JobCallback;
import com.enderage.core.scheduler.main.Task;
import com.enderage.core.scheduler.tasks.BasicAsyncTask;
import com.enderage.core.scheduler.tasks.DBFindQuery;
import com.enderage.core.scheduler.tasks.DBMultiUpdateQuery;
import com.enderage.core.utils.PlayerUtils;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * 
 * @author TwistPvP
 */
public class PlayerManager {

	private ConcurrentHashMap<Player, EnderPlayer> playerMapping;

	private ConcurrentHashMap<String, Task> playerUnloadTasks;

	public PlayerManager() {
		playerUnloadTasks = new ConcurrentHashMap<String, Task>();
		playerMapping = new ConcurrentHashMap<Player, EnderPlayer>();
	}

	public void initialize() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			loadEnderPlayer(p, new IPlayerLoaded() {

				@Override
				public void onPlayerLoaded(Player p, EnderPlayer ep) {
					setTablistColor(ep);
				}
			});
		}
	}

	private EnderPlayer tryGetCachedName(String username) {
		EnderPlayer f = null;
		for (EnderPlayer ep : playerMapping.values()) {
			if (ep.getUsername().equalsIgnoreCase(username)) {
				f = ep;
				break;
			}
		}
		return f;
	}

	private EnderPlayer tryGetCachedUuid(String uuid) {
		EnderPlayer f = null;
		for (EnderPlayer ep : playerMapping.values()) {
			if (ep.getUuid() == uuid) {
				f = ep;
				break;
			}
		}
		return f;
	}

	public void setTablistColor(EnderPlayer ep) {
		String nick = ep.getNick();
		if (nick.length() > 14) {
			nick = nick.substring(0, 14);
		}
		ep.getPlayer().setPlayerListName(
				ep.getPreferedPermission().getColor() + nick);
	}

	public EnderPlayer getEnderPlayer(Player p) {
		return playerMapping.get(p);
	}

	public EnderPlayer[] getAll() {
		return playerMapping.values().toArray(new EnderPlayer[0]);
	}

	public void stop() {
		for (EnderPlayer ep : playerMapping.values()) {
			DBObject obj = getSavePlayerObject(ep);
			Core.get().getDatabaseManager().getCollection(Collection.PLAYERS)
					.update(new BasicDBObject("uuid", ep.getUuid()), obj);
		}
		playerMapping.clear();
	}

	/**
	 * Loads in the same thread, pretty slow!
	 * 
	 * @param uuid
	 *            The player's uuid without -'s
	 * @return SparkPlayer instance
	 */
	public EnderPlayer loadEnderPlayerSync(final String uuid) {
		EnderPlayer c = tryGetCachedUuid(uuid);
		if (c != null) {
			return c;
		}
		DBObject obj = Core.get().getDatabaseManager()
				.getCollection(Collection.PLAYERS)
				.findOne(new BasicDBObject("uuid", uuid));
		if (obj == null) {
			return null;
		}
		return fromDBObject(obj);
	}

	public EnderPlayer findEnderPlayerSync(final String username) {
		EnderPlayer c = tryGetCachedName(username);
		if (c != null) {
			return c;
		}
		DBObject obj = Core
				.get()
				.getDatabaseManager()
				.getCollection(Collection.PLAYERS)
				.findOne(
						new BasicDBObject("lookupUsername", username
								.toLowerCase()));
		if (obj == null) {
			return null;
		}
		return fromDBObject(obj);
	}

	public void reloadPlayer(final Player p, final JobCallback callback) {
		Core.get()
				.getTaskManager()
				.startTask(
						new DBFindQuery(Core.get(), Collection.PLAYERS,
								new BasicDBObject("uuid", PlayerUtils
										.getStringUuid(p)),
								new JobCallback<DBFindQuery>() {

									@Override
									public void onDone(int taskId,
											DBFindQuery obj) {
										if (obj.getResult() != null) {
											EnderPlayer ep = fromDBObject(obj
													.getResult());
											if (ep != null) {
												if (playerMapping
														.containsKey(p)) {
													playerMapping.remove(p);
												}
												playerMapping.put(p, ep);
												setTablistColor(ep);
												callback.onDone(taskId, obj);
												return;
											}
										}
										callback.onAbort(taskId, obj);
									}

									@Override
									public void onAbort(int taskId,
											DBFindQuery obj) {
										callback.onAbort(taskId, obj);
									}
								}));
	}

	public void findEnderPlayer(final String username,
			final ISparkPlayerFound callback) {
		EnderPlayer c = tryGetCachedName(username);
		if (c != null) {
			callback.onSparkPlayerFound(c);
			return;
		}
		Core.get()
				.getTaskManager()
				.startTask(
						new DBFindQuery(Core.get(), Collection.PLAYERS,
								new BasicDBObject("lookupUsername", username
										.toLowerCase()),
								new JobCallback<DBFindQuery>() {

									@Override
									public void onDone(int taskId,
											DBFindQuery obj) {
										if (obj.getResult() != null) {
											callback.onSparkPlayerFound(fromDBObject(obj
													.getResult()));
										}
									}

									@Override
									public void onAbort(int taskId,
											DBFindQuery obj) {

									}
								}));
	}

	public void findEnderPlayerByUUID(final String uuid,
			final ISparkPlayerFound callback) {
		EnderPlayer c = tryGetCachedUuid(uuid);
		if (c != null) {
			callback.onSparkPlayerFound(c);
			return;
		}
		Core.get()
				.getTaskManager()
				.startTask(
						new DBFindQuery(Core.get(), Collection.PLAYERS,
								new BasicDBObject("uuid", uuid),
								new JobCallback<DBFindQuery>() {

									@Override
									public void onDone(int taskId,
											DBFindQuery obj) {
										if (obj.getResult() != null) {
											callback.onSparkPlayerFound(fromDBObject(obj
													.getResult()));
										}
									}

									@Override
									public void onAbort(int taskId,
											DBFindQuery obj) {

									}
								}));
	}

	private EnderPlayer fromCache(String uuid) {
		if (playerUnloadTasks.contains(uuid)) {
			Task t = playerUnloadTasks.remove(uuid);
			t.abort();
		}
		EnderPlayer ep = null;
		for (Entry<Player, EnderPlayer> e : playerMapping.entrySet()) {
			if (PlayerUtils.getStringUuid(e.getKey()).equals(uuid)
					|| e.getKey().getName().equalsIgnoreCase(uuid)) {
				ep = e.getValue();
				break;
			}
		}
		return ep;
	}

	private EnderPlayer fromCache(Player p) {
		return fromCache(PlayerUtils.getStringUuid(p));
	}

	public void loadEnderPlayer(final Player w, final IPlayerLoaded callback) {
		EnderPlayer ep = fromCache(w);
		if (ep != null) {
			callback.onPlayerLoaded(w, ep);
			return;
		}
		final String uuid = PlayerUtils.getStringUuid(w);
		Core.get()
				.getTaskManager()
				.startTask(
						new DBFindQuery(Core.get(), Core.get()
								.getDatabaseManager()
								.getCollection(Collection.PLAYERS),
								new BasicDBObject("uuid", uuid),
								new JobCallback<DBFindQuery>() {

									@Override
									public void onDone(int taskId,
											DBFindQuery obj) {
										DBObject result = obj.getResult();
										if (result == null) {
											insertNewPlayer(uuid, w.getName(),
													callback);
											return;
										}
										EnderPlayer ep = fromDBObject(result);
										if (ep == null) {
											w.kickPlayer("Your player data is corrupt, message an admin about the issue");
											return;
										}
										if (!playerMapping.contains(w)) {
											playerMapping.put(w, sp);
										}
										if (callback != null) {
											callback.onPlayerLoaded(w, sp);
										}
										if (sp.getUsername() != w.getName()) {
											sp.setUsername(w.getName());
											savePlayer(sp);
										}
									}

									@Override
									public void onAbort(int taskId,
											DBFindQuery obj) {
										w.kickPlayer("The server aborted your playerdata loading!");
									}

								}));
	}

	public void loadEnderPlayer(final String uuid, final IPlayerLoaded callback) {
		loadEnderPlayer(PlayerUtils.getByUUID(uuid), callback);
	}

	private EnderPlayer fromDBObject(DBObject obj) {
		if (obj.containsField("uuid") && obj.containsField("username")) {
			try {
				String uuid = (String) obj.get("uuid");
				String username = (String) obj.get("username");
				Constructor<EnderPlayer> cns = EnderPlayer.class
						.getDeclaredConstructor(String.class, String.class);
				cns.setAccessible(true);
				EnderPlayer sp = (EnderPlayer) cns.newInstance(uuid, username);
				if (obj.containsField("nick")) {
					sp.setNick((String) obj.get("nick"));
				}
				if (obj.containsField("metadata")) {
					BasicDBList arr = (BasicDBList) obj.get("metadata");
					for (Object o : arr) {
						try {
							DBObject ok = (DBObject) o;
							sp.setMetadata((String) ok.get("key"),
									ok.get("value"));
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
				}
				return sp;
			} catch (NoSuchMethodException | SecurityException
					| InstantiationException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException ex) {
				Logger.getLogger(PlayerManager.class.getName()).log(
						Level.SEVERE, null, ex);
			}
		}
		return null;
	}

	private void insertNewPlayer(String uuid, String username,
			final IPlayerLoaded callback) {
		BasicDBObject obj = new BasicDBObject();

		obj.put("uuid", uuid);
		obj.put("username", username);
		obj.put("lookupUsername", username.toLowerCase());
		obj.put("nick", username);

		BasicDBList meta = new BasicDBList();
		obj.put("metadata", meta);

		Core.get().getDatabaseManager().getCollection(Collection.PLAYERS)
				.insert(obj);
		loadEnderPlayer(uuid, callback);
	}

	public void queUnloadPlayer(final Player p) {
		playerUnloadTasks.put(
				PlayerUtils.getStringUuid(p),
				Core.get()
						.getTaskManager()
						.startTaskLater(
								new BasicAsyncTask(Core.get(),
										new JobCallback() {

											@Override
											public void onDone(int taskId,
													Task obj) {
												unloadPlayer(p);
											}

											@Override
											public void onAbort(int taskId,
													Task obj) {

											}

										}), 20 * 60));
	}

	public void unloadPlayer(Player p) {
		this.playerMapping.remove(p);
	}

	public void forceUnloadAll() {
		playerMapping.clear();
	}

	public void savePlayer(EnderPlayer ep) {
		savePlayer(ep, 0);
	}

	private DBObject getSavePlayerObject(EnderPlayer ep) {
		BasicDBObject update = new BasicDBObject();
		update.put("username", ep.getUsername());
		update.put("nick", ep.getNick());
		update.put("lookupUsername", ep.getUsername().toLowerCase());
		BasicDBList list = new BasicDBList();
		for (Setting o : ep.getMetadata()) {
			list.add(o.DBserialize());
		}
		update.put("metadata", list);
		return update;
	}

	private void savePlayer(final EnderPlayer ep, final int t) {
		DBObject update = getSavePlayerObject(sp);

		Core.get()
				.getTaskManager()
				.startTask(
						new DBMultiUpdateQuery(Core.get(), Collection.PLAYERS,
								new BasicDBObject("uuid", ep.getUuid()),
								update, new ExceptionJobCallback() {

									@Override
									public void onDone(int taskId, Task obj) {

									}

									@Override
									public void onAbort(int taskId, Task obj) {
									}

									@Override
									public void onException(int taskId,
											Task obj, Exception ex) {
										if (t < 3) {
											savePlayer(ep, t + 1);
										} else {
											Core.get().Log(
													ChatColor.RED,
													"Couldn't save player data for "
															+ ChatColor.AQUA
															+ ep.getUsername());
											ex.printStackTrace();
										}
									}

								}));
	}
}
