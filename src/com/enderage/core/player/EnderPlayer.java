package com.enderage.core.player;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.enderage.core.Core;
import com.enderage.core.database.instance.Setting;
import com.enderage.core.perms.PermissionGroup;
import com.enderage.core.utils.PlayerUtils;
import com.mongodb.BasicDBList;

/**
 * 
 * @author TwistPvP
 */
public class EnderPlayer {

	private String username;
	private String nick;
	private String uuid;
	private ArrayList<Setting> metadata;

	private EnderPlayer(String uuid, String username) {
		this.uuid = uuid;
		this.username = username;
		this.metadata = new ArrayList<Setting>();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String name) {
		this.username = name;
	}

	public String getNick() {
		return nick;
	}

	public String getUuid() {
		return uuid;
	}

	public ArrayList<Setting> getMetadata() {
		return metadata;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public void setAllMetadata(ArrayList<Setting> metadata) {
		this.metadata = metadata;
	}

	public void setMetadata(String key, Object value) {
		if (hasMetadata(key)) {
			removeMetadata(key);
		}
		this.metadata.add(new Setting(key, value));
	}

	public Object getMetadata(String key) {
		Object f = null;
		for (Setting o : this.metadata) {
			if (o.getName().equals(key)) {
				f = o.getValue();
				break;
			}
		}
		return f;
	}

	public void removeMetadata(String key) {
		Setting f = null;
		for (Setting k : metadata) {
			if (k.getName().equals(key)) {
				f = k;
				break;
			}
		}
		if (f != null) {
			this.metadata.remove(f);
		}
	}

	public boolean hasMetadata(String key) {
		return (this.getMetadata(key) != null);
	}

	public void removePermission(String alias) {
		if (hasMetadata("rank")) {
			List<String> pg = new ArrayList<String>();
			for (Object o : (BasicDBList) getMetadata("rank")) {
				if (!((String) o).equals(alias)) {
					pg.add((String) o);
				}
			}
			this.setMetadata("rank", pg);
		}
		if (this.getPreferedPermission().getAlias().equals(alias)) {
			this.setPreferedPermission(getPermissions()[0]);
		}
	}

	public PermissionGroup[] getPermissions() {
		if (hasMetadata("rank")) {
			List<PermissionGroup> ps = new ArrayList<PermissionGroup>();
			BasicDBList l = (BasicDBList) getMetadata("rank");
			for (Object o : l) {
				ps.add(Core.getPermManager().getByAlias((String) o));
			}
			return ps.toArray(new PermissionGroup[0]);
		}
		return new PermissionGroup[] { Core.getPermManager()
				.getDefaultPermission() };
	}

	public void addPermission(PermissionGroup group) {
		addPermission(group.getAlias());
	}

	public void addPermission(String name) {
		Object p = getMetadata("rank");
		BasicDBList list = null;
		if (p != null && p instanceof BasicDBList) {
			list = (BasicDBList) p;
		} else {
			list = new BasicDBList();
		}
		list.add(name);
		setMetadata("rank", list);
	}

	public boolean setPreferedPermission(PermissionGroup group) {
		for (PermissionGroup p : getPermissions()) {
			if (p == group) {
				setMetadata("shownRank", group.getAlias());
				return true;
			}
		}
		return false;
	}

	public PermissionGroup getPreferedPermission() {
		try {
			if (getMetadata("shownRank") != null) {
				return Core.getPermManager().getByAlias(
						(String) getMetadata("shownRank"));
			}

		} catch (Exception ex) {
		}
		return Core.getPermManager().getDefaultPermission();
	}

	public Player getPlayer() {
		return Bukkit.getPlayer(PlayerUtils.getBukkitUuid(getUuid()));
	}
}
