package com.enderage.core.callbacks;

import org.bukkit.entity.Player;

import com.enderage.core.player.EnderPlayer;

/**
 * 
 * @author TwistPvP
 */
public interface IPlayerLoaded {

	public void onPlayerLoaded(Player p, EnderPlayer ep);
}
