package com.enderage.core.callbacks;

/**
 * 
 * @author TwistPvP
 */
public interface IDBInstanceLoadedCallback {

	public void InstanceLoadedCallback();
}
