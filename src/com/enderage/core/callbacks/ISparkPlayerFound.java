/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.enderage.core.callbacks;

import com.enderage.core.player.EnderPlayer;

/**
 * 
 * @author TwistPvP
 */
public interface ISparkPlayerFound {

	public void onSparkPlayerFound(EnderPlayer ep);
}
