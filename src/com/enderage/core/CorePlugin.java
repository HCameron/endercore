package com.enderage.core;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * 
 * @author TwistPvP
 */
public abstract class CorePlugin extends JavaPlugin {

	public abstract void Log(ChatColor c, String message);
}
