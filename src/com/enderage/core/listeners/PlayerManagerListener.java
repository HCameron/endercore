package com.enderage.core.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.enderage.core.Core;
import com.enderage.core.callbacks.IPlayerLoaded;
import com.enderage.core.events.EnderPlayerLoadedEvent;
import com.enderage.core.player.EnderPlayer;

/**
 * 
 * @author TwistPvP
 */
public class PlayerManagerListener implements Listener {

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Core.getPlayerManager().loadEnderPlayer(event.getPlayer(),
				new IPlayerLoaded() {

					@Override
					public void onPlayerLoaded(Player p, EnderPlayer ep) {
						EnderPlayerLoadedEvent event = new EnderPlayerLoadedEvent(
								p, ep);
						Bukkit.getPluginManager().callEvent(event);
						Core.getPlayerManager().setTablistColor(ep);
					}

				});
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		Core.getPlayerManager().savePlayer(
				Core.getPlayerManager().getEnderPlayer(event.getPlayer()));
		Core.getPlayerManager().queUnloadPlayer(event.getPlayer());
	}

	@EventHandler
	public void onPlayerChatAsync(AsyncPlayerChatEvent event) {
		try {
			String prefix = "";
			String suffix = "";
			EnderPlayer sp = Core.getPlayerManager().getEnderPlayer(
					event.getPlayer());
			if (sp == null) {
				event.setCancelled(true);
				return;
			}
			ChatColor c = ChatColor.YELLOW;
			if (sp.getPreferedPermission() != null
					&& !sp.getPreferedPermission().isDefault()) {
				prefix = Core.get().getSetting("prefixFormat").asString();
				String pr = sp.getPreferedPermission().getDisplayName();
				if (sp.hasMetadata("prefix")) {
					pr = (String) sp.getMetadata("prefix");
				}
				prefix = prefix.replace("%prefix%", pr);
			}
			c = sp.getPreferedPermission().getColor();
			if (sp.hasMetadata("color")) {
				c = ChatColor.valueOf(sp.getMetadata("color").toString());
			}
			if (sp.hasMetadata("suffix")) {
				suffix = Core
						.get()
						.getSetting("suffixFormat")
						.asString()
						.replace("%suffix%",
								sp.getMetadata("suffix").toString());
			}

			String format = Core.get().getSetting("chatFormat").asString();
			format = format.replace("%prefix%", prefix);
			format = format.replace("%username%", c + sp.getNick());
			format = format.replace("%suffix%", suffix);

			System.out.println(format);

			format = ChatColor.translateAlternateColorCodes('&', format);

			event.setFormat(format + ChatColor.WHITE + " " + event.getMessage());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
