package com.enderage.core;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.enderage.core.cmd.CommandLoader;
import com.enderage.core.database.DBInstanceLoader;
import com.enderage.core.database.DatabaseManager;
import com.enderage.core.database.instance.Setting;
import com.enderage.core.fxmanager.PotionManager;
import com.enderage.core.listeners.PlayerManagerListener;
import com.enderage.core.perms.PermissionManager;
import com.enderage.core.player.PlayerManager;
import com.enderage.core.scheduler.main.TaskManager;
import com.enderage.core.utils.PropertiesFileManager;

/**
 * 
 * @author TwistPvP
 * 
 */
public class Core extends CorePlugin {

	private static Core plugin;

	public static final String CORE_PREFIX = ChatColor.DARK_RED + "["
			+ ChatColor.RED + "EnderCore" + ChatColor.DARK_RED + "] ";
	public static final String CONSOLE_PREFIX = ChatColor.WHITE
			+ "[EnderCore] ";
	public static final String LOG_PREFIX = "[EnderCore] ";

	public String PERMISSION_PREFIX = "core";
	public String COMMAND_PREFIX = "sparkeh";

	private static PlayerManager playerManager;
	private CommandLoader commandLoader;
	private static PermissionManager permManager;
	private DatabaseManager databaseManager;
	private PropertiesFileManager PFM;
	private DBInstanceLoader coreInstanceLoader;
	private Settings settings;
	private static PotionManager potionManager;
	private TaskManager taskManager;
	private OverridableTools tools;

	@Override
	public void onEnable() {
		plugin = this;
		try {
			this.PFM = new PropertiesFileManager();
			this.databaseManager = new DatabaseManager();
			this.databaseManager.initialize(PFM
					.getPropertiesFile("database.config"));
			this.coreInstanceLoader = new DBInstanceLoader(
					this.databaseManager.getDefaultDB());
			this.settings = new Settings();
			this.settings.initialize(this.coreInstanceLoader);
			this.taskManager = new TaskManager();
			permManager = new PermissionManager();
			permManager.initialize(this.coreInstanceLoader);
			playerManager = new PlayerManager();
			playerManager.initialize();
			this.commandLoader = new CommandLoader();
			this.commandLoader.loadPluginCommands(this, "EnderCore",
					"com.enderage.core.cmd.commands");
			potionManager = new PotionManager();
			this.tools = new CoreTools();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		Bukkit.getPluginManager().registerEvents(new PlayerManagerListener(),
				this);

		PFM.SaveAll();
	}

	@Override
	public void onDisable() {
		this.taskManager.endAll();
		playerManager.stop();
		permManager.saveAllGroups(false);
	}

	public static Core get() {
		return plugin;
	}

	public static PotionManager getPotionManager() {
		return potionManager;
	}

	public String getGamePrefix() {
		return CORE_PREFIX;
	}

	public static PlayerManager getPlayerManager() {
		return playerManager;
	}

	public void broadcastOps(String message) {
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (p.isOp()) {
				p.sendMessage(message);
			}
		}
	}

	@Deprecated
	public static void _Log(ChatColor c, String message) {
		Core.get().getServer().getConsoleSender()
				.sendMessage(CONSOLE_PREFIX + c + message);
	}

	@Override
	public void Log(ChatColor c, String message) {
		Core.get().getServer().getConsoleSender()
				.sendMessage(CONSOLE_PREFIX + c + message);
	}

	public static PermissionManager getPermManager() {
		return permManager;
	}

	public DatabaseManager getDatabaseManager() {
		return this.databaseManager;
	}

	public PropertiesFileManager getPropertiesFileManager() {
		return this.PFM;
	}

	public DBInstanceLoader getCoreInstanceLoader() {
		return this.coreInstanceLoader;
	}

	public Settings getSettings() {
		return this.settings;
	}

	public TaskManager getTaskManager() {
		return this.taskManager;
	}

	public OverridableTools getTools() {
		return this.tools;
	}

	public void setTools(OverridableTools tools) {
		this.tools = tools;
	}

	public CommandLoader getCommandLoader() {
		return this.commandLoader;
	}

	public Setting getSetting(String key) {
		return getSettings().get(key);
	}
}
