package com.enderage.core.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.enderage.core.player.EnderPlayer;

/**
 * 
 * @author TwistPvP
 */
public class EnderPlayerLoadedEvent extends Event {

	private static final HandlerList handlers = new HandlerList();
	private final Player player;
	private final EnderPlayer enderPlayer;

	public EnderPlayerLoadedEvent(Player player, EnderPlayer enderPlayer) {
		this.player = player;
		this.enderPlayer = enderPlayer;
	}

	public EnderPlayer getSparkPlayer() {
		return this.enderPlayer;
	}

	public Player getPlayer() {
		return this.player;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
