package com.enderage.core.scheduler.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.enderage.core.Core;
import com.enderage.core.scheduler.callbacks.DelayedPausableTickExceptionCallback;
import com.enderage.core.scheduler.callbacks.JobCallback;
import com.enderage.core.scheduler.callbacks.PausableJobCallback;
import com.enderage.core.scheduler.callbacks.TickCallback;

/**
 * 
 * @author TwistPvP
 */
public abstract class Task implements Runnable {

	private JavaPlugin owner;
	private int taskId;
	private int waitTaskId;
	@SuppressWarnings("rawtypes")
	private JobCallback callback;

	private boolean paused;

	@SuppressWarnings("rawtypes")
	public Task(JavaPlugin owner, JobCallback callback) {
		this.owner = owner;
		this.callback = callback;
		waitTaskId = -1;
		taskId = -1;
		this.paused = false;
	}

	public void Start() {
		this.taskId = doTaskStart(owner, callback);
	}

	public void StartLater(long delayTicks) {
		final Task me = this;
		this.waitTaskId = Bukkit.getScheduler().scheduleSyncDelayedTask(owner,
				new Runnable() {
					@Override
					public void run() {
						me.Start();
						me.waitTaskId = -1;
					}
				}, delayTicks);
	}

	@SuppressWarnings("rawtypes")
	protected abstract int doTaskStart(JavaPlugin owner, JobCallback callback);

	protected abstract void abortTask();

	protected abstract boolean allowPause();

	protected abstract void pauseTask();

	protected abstract void resumeTask();

	public int getWaitTaskId() {
		return this.waitTaskId;
	}

	public int getTaskId() {
		return this.taskId;
	}

	public void setTaskId(int id) {
		this.taskId = id;
	}

	public void setWaitTaskId(int id) {
		this.waitTaskId = id;
	}

	public boolean isPaused() {
		return this.paused;
	}

	@SuppressWarnings("unchecked")
	public void setPaused(boolean paused) {
		if (this.allowPause()) {
			this.paused = paused;
			if (paused) {
				pauseTask();
				getCallback(PausableJobCallback.class).onPause(getTaskId(),
						this);
			} else {
				resumeTask();
				getCallback(PausableJobCallback.class).onResume(getTaskId(),
						this);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	public <T extends JobCallback> T getCallback(Class<T> type) {
		if (this.callback != null && type.isInstance(this.callback)) {
			return type.cast(this.callback);
		}
		return type.cast(new EmptyCallback());
	}

	@SuppressWarnings("unchecked")
	public void abort() {
		getCallback(TickCallback.class).onAbort(getTaskId(), this);
		endTask();
	}

	protected void endTask() {
		try {
			Bukkit.getScheduler().cancelTask(this.getTaskId());
			Bukkit.getScheduler().cancelTask(this.getWaitTaskId());
		} catch (Exception ex) {

		}
		Core.get().getTaskManager().markTaskDone(this);
	}

	public JavaPlugin getOwner() {
		return this.owner;
	}

	/**
	 * 
	 * @author TwistPvP
	 * 
	 */
	@SuppressWarnings("rawtypes")
	private class EmptyCallback implements DelayedPausableTickExceptionCallback {

		@Override
		public void onStart(int taskId, Task obj) {
		}

		@Override
		public void onDone(int taskId, Task obj) {
		}

		@Override
		public void onAbort(int taskId, Task obj) {
		}

		@Override
		public void onTick(int taskId, int step, Task obj) {
		}

		@Override
		public void onPause(int taskId, Task obj) {
		}

		@Override
		public void onResume(int taskId, Task obj) {
		}

		@Override
		public void onException(int taskId, Task obj, Exception ex) {

		}
	}
}
