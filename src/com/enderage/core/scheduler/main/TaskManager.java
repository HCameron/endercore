package com.enderage.core.scheduler.main;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * 
 * @author TwistPvP
 */
public class TaskManager {

	private ArrayList<Task> tasks;

	public TaskManager() {
		tasks = new ArrayList<Task>();
	}

	protected void markTaskDone(Task t) {
		tasks.remove(t);
	}

	public Task startTask(Task t) {
		if (!this.tasks.contains(t)) {
			this.tasks.add(t);
			t.Start();
			return t;
		}
		return null;
	}

	public Task startTaskLater(Task t, long delayTicks) {
		if (!this.tasks.contains(t)) {
			this.tasks.add(t);
			t.StartLater(delayTicks);
			return t;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public void endAll() {
		for (Task t : (ArrayList<Task>) tasks.clone()) {
			t.endTask();
		}
		tasks.clear();
	}

	public void pauseAll() {
		for (Task t : tasks) {
			t.setPaused(true);
		}
	}

	public void resumeAll() {
		for (Task t : tasks) {
			t.setPaused(false);
		}
	}

	public Task getTask(int bukkitId) {
		Task f = null;
		for (Task t : tasks) {
			if (t.getTaskId() == bukkitId) {
				f = t;
				break;
			}
		}
		return f;
	}

	public void pauseTask(Task t) {
		t.setPaused(true);
	}

	public void resumeTask(Task t) {
		t.setPaused(false);
	}

	public ArrayList<Task> getAll() {
		return tasks;
	}

	public List<Task> getForPlugin(JavaPlugin plugin) {
		List<Task> ret = new ArrayList<Task>();
		for (Task t : tasks) {
			if (t.getOwner() == plugin) {
				ret.add(t);
			}
		}
		return ret;
	}
}
