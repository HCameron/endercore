package com.enderage.core.scheduler.callbacks;

import com.enderage.core.scheduler.main.Task;

/**
 * 
 * @author TwistPvP
 */
public interface ExceptionJobCallback<T extends Task> extends JobCallback<T> {

	public void onException(int taskId, T obj, Exception ex);
}
