package com.enderage.core.scheduler.callbacks;

import com.enderage.core.scheduler.main.Task;

/**
 * 
 * @author TwistPvP
 */
public interface TickCallback<T extends Task> extends JobCallback<T> {

	public void onTick(int taskId, int step, T obj);
}
