package com.enderage.core.scheduler.callbacks;

import com.enderage.core.scheduler.main.Task;

/**
 * 
 * @author TwistPvP
 */
public interface DelayedPausableJobCallback<T extends Task> extends
		DelayedJobCallback<T>, PausableJobCallback<T> {
}