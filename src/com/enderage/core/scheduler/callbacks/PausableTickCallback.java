package com.enderage.core.scheduler.callbacks;

import com.enderage.core.scheduler.main.Task;

/**
 * 
 * @author TwistPvP
 */
public interface PausableTickCallback<T extends Task> extends
		PausableJobCallback<T>, TickCallback<T> {
}
