package com.enderage.core.scheduler.callbacks;

import com.enderage.core.scheduler.main.Task;

/**
 * 
 * @author TwistPvP
 */
public interface DelayedTickCallback<T extends Task> extends
		DelayedJobCallback<T>, TickCallback<T> {
}
