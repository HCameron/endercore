package com.enderage.core.scheduler.callbacks;

import com.enderage.core.scheduler.main.Task;

/**
 * 
 * @author TwistPvP
 */
public interface JobCallback<T extends Task> {

	public void onDone(int taskId, T obj);

	public void onAbort(int taskId, T obj);
}
