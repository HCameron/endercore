package com.enderage.core.scheduler.callbacks;

import com.enderage.core.scheduler.main.Task;

/**
 * 
 * @author TwistPvP
 */
public interface PausableJobCallback<T extends Task> extends JobCallback<T> {

	public void onPause(int taskId, T obj);

	public void onResume(int taskId, T obj);
}
