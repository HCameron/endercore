package com.enderage.core.scheduler.callbacks;

import com.enderage.core.scheduler.main.Task;

/**
 * 
 * @author TwistPvP
 */
public interface DelayedJobCallback<T extends Task> extends JobCallback<T> {

	public void onStart(int taskId, T obj);
}
