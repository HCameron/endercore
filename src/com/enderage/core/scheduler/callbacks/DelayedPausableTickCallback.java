package com.enderage.core.scheduler.callbacks;

import com.enderage.core.scheduler.main.Task;

/**
 * 
 * @author TwistPvP
 */
public interface DelayedPausableTickCallback<T extends Task> extends
		DelayedTickCallback<T>, PausableTickCallback<T> {
}
