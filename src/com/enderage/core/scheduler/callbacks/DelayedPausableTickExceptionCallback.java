package com.enderage.core.scheduler.callbacks;

import com.enderage.core.scheduler.main.Task;

/**
 * Actually not really a useful callback class, it's just used in Task class,
 * because it implements all callback's functions for the
 * super-duper-empty-callback class
 * 
 * @author TwistPvP
 */
public interface DelayedPausableTickExceptionCallback<T extends Task> extends
		DelayedPausableTickCallback<T>, ExceptionJobCallback<T> {

}
