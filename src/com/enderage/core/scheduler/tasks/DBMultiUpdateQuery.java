package com.enderage.core.scheduler.tasks;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.enderage.core.Core;
import com.enderage.core.database.Collection;
import com.enderage.core.scheduler.callbacks.JobCallback;
import com.enderage.core.scheduler.main.Task;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

/**
 * 
 * @author TwistPvP
 */
public class DBMultiUpdateQuery extends Task {

	private final DBCollection col;
	private final DBObject query;
	private final DBObject updates;

	@SuppressWarnings("rawtypes")
	public DBMultiUpdateQuery(JavaPlugin owner, DBCollection col,
			DBObject query, DBObject keyValues, JobCallback callback) {
		super(owner, callback);
		this.col = col;
		this.query = query;
		this.updates = keyValues;
	}

	@SuppressWarnings("rawtypes")
	public DBMultiUpdateQuery(JavaPlugin owner, Collection col, DBObject query,
			DBObject keyValues, JobCallback callback) {
		this(owner, Core.get().getDatabaseManager().getCollection(col), query,
				keyValues, callback);
	}

	@SuppressWarnings({ "deprecation", "rawtypes" })
	@Override
	protected int doTaskStart(JavaPlugin owner, JobCallback callback) {
		return Bukkit.getScheduler().scheduleAsyncDelayedTask(owner, this);
	}

	@Override
	protected void abortTask() {

	}

	@Override
	protected boolean allowPause() {
		return false;
	}

	@Override
	protected void pauseTask() {

	}

	@Override
	protected void resumeTask() {

	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		col.update(query, new BasicDBObject("$set", this.updates));
		getCallback(JobCallback.class).onDone(getTaskId(), this);
	}
}
