package com.enderage.core.scheduler.tasks;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.enderage.core.scheduler.callbacks.DelayedTickCallback;
import com.enderage.core.scheduler.callbacks.JobCallback;
import com.enderage.core.scheduler.callbacks.TickCallback;
import com.enderage.core.scheduler.main.Task;

/**
 * 
 * @author TwistPvP
 */
public class CountdownTask extends Task {

	private int start;
	private int pos;

	@SuppressWarnings("rawtypes")
	public CountdownTask(JavaPlugin owner, int start, JobCallback callback) {
		super(owner, callback);
		this.start = start;
		this.pos = start;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected int doTaskStart(JavaPlugin owner, JobCallback callback) {
		return Bukkit.getScheduler().scheduleSyncRepeatingTask(owner, this, 0L,
				20L);
	}

	public int getCurrentPosition() {
		return this.pos;
	}

	public void setPosition(int position) {
		this.pos = position;
	}

	public int getTotalWaitTime() {
		return this.start;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		if (this.isPaused()) {
			return;
		}
		if (this.pos == this.start) {
			getCallback(DelayedTickCallback.class).onStart(getTaskId(), this);
		} else if (this.pos > 0) {
			getCallback(TickCallback.class).onTick(getTaskId(), this.start,
					this);
		} else {
			getCallback(TickCallback.class).onDone(getTaskId(), this);
			endTask();
		}
		this.pos--;
	}

	@Override
	protected void abortTask() {

	}

	@Override
	protected boolean allowPause() {
		return true;
	}

	@Override
	protected void pauseTask() {
	}

	@Override
	protected void resumeTask() {
	}
}
