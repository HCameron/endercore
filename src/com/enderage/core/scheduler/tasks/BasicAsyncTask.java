package com.enderage.core.scheduler.tasks;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.enderage.core.scheduler.callbacks.JobCallback;
import com.enderage.core.scheduler.main.Task;

/**
 * 
 * @author TwistPvP
 */
public class BasicAsyncTask extends Task {

	@SuppressWarnings("rawtypes")
	public BasicAsyncTask(JavaPlugin owner, JobCallback callback) {
		super(owner, callback);
	}

	@SuppressWarnings({ "deprecation", "rawtypes" })
	@Override
	protected int doTaskStart(JavaPlugin owner, JobCallback callback) {
		return Bukkit.getScheduler().scheduleAsyncDelayedTask(owner, this);
	}

	@Override
	protected void abortTask() {
	}

	@Override
	protected boolean allowPause() {
		return false;
	}

	@Override
	protected void pauseTask() {
	}

	@Override
	protected void resumeTask() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		getCallback(JobCallback.class).onDone(getTaskId(), this);
	}
}
