package com.enderage.core.scheduler.tasks;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.enderage.core.Core;
import com.enderage.core.database.Collection;
import com.enderage.core.scheduler.callbacks.JobCallback;
import com.enderage.core.scheduler.main.Task;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

/**
 * 
 * @author TwistPvP
 */
public class DBDeleteQuery extends Task {

	private DBCollection col;
	private DBObject query;

	@SuppressWarnings("rawtypes")
	public DBDeleteQuery(JavaPlugin owner, DBCollection col, DBObject query,
			JobCallback callback) {
		super(owner, callback);
		this.col = col;
		this.query = query;
	}

	@SuppressWarnings("rawtypes")
	public DBDeleteQuery(JavaPlugin owner, Collection col, DBObject query,
			JobCallback callback) {
		this(owner, Core.get().getDatabaseManager().getCollection(col), query,
				callback);
	}

	@SuppressWarnings({ "deprecation", "rawtypes" })
	@Override
	protected int doTaskStart(JavaPlugin owner, JobCallback callback) {
		return Bukkit.getScheduler().scheduleAsyncDelayedTask(owner, this);
	}

	@Override
	protected void abortTask() {

	}

	@Override
	protected boolean allowPause() {
		return false;
	}

	@Override
	protected void pauseTask() {

	}

	@Override
	protected void resumeTask() {

	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		col.remove(query);
		getCallback(JobCallback.class).onDone(getTaskId(), this);
	}
}
