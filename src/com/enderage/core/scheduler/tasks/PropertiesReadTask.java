package com.enderage.core.scheduler.tasks;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.enderage.core.scheduler.callbacks.ExceptionJobCallback;
import com.enderage.core.scheduler.callbacks.JobCallback;
import com.enderage.core.scheduler.main.Task;
import com.enderage.core.utils.PropertiesFile;

/**
 * 
 * @author TwistPvP
 */
public class PropertiesReadTask extends Task {

	private PropertiesFile file;

	@SuppressWarnings("rawtypes")
	public PropertiesReadTask(JavaPlugin owner, PropertiesFile file,
			JobCallback callback) {
		super(owner, callback);
		this.file = file;
	}

	public PropertiesFile getPropertiesFile() {
		return this.file;
	}

	@SuppressWarnings({ "deprecation", "rawtypes" })
	@Override
	protected int doTaskStart(JavaPlugin owner, JobCallback callback) {
		return Bukkit.getScheduler().scheduleAsyncDelayedTask(owner, this);
	}

	@Override
	protected void abortTask() {

	}

	@Override
	protected boolean allowPause() {
		return false;
	}

	@Override
	protected void pauseTask() {

	}

	@Override
	protected void resumeTask() {

	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		try {
			file.load();
			getCallback(JobCallback.class).onDone(getTaskId(), this);
		} catch (Exception ex) {
			getCallback(ExceptionJobCallback.class).onException(getTaskId(),
					this, ex);
		}
	}
}
