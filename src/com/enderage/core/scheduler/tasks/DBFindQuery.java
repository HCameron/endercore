package com.enderage.core.scheduler.tasks;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.enderage.core.Core;
import com.enderage.core.database.Collection;
import com.enderage.core.scheduler.callbacks.JobCallback;
import com.enderage.core.scheduler.main.Task;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

/**
 * 
 * @author TwistPvP
 */
public class DBFindQuery extends Task {

	private final DBCollection col;
	private final DBObject query;
	private final DBObject keys;

	private DBObject result;

	@SuppressWarnings("rawtypes")
	public DBFindQuery(JavaPlugin owner, DBCollection col, DBObject query,
			JobCallback callback) {
		this(owner, col, query, null, callback);
	}

	@SuppressWarnings("rawtypes")
	public DBFindQuery(JavaPlugin owner, DBCollection col, DBObject query,
			DBObject keys, JobCallback callback) {
		super(owner, callback);
		this.query = query;
		this.col = col;
		this.keys = keys;
	}

	@SuppressWarnings("rawtypes")
	public DBFindQuery(JavaPlugin owner, Collection col, DBObject query,
			JobCallback callback) {
		this(owner, Core.get().getDatabaseManager().getCollection(col), query,
				callback);
	}

	@SuppressWarnings("rawtypes")
	public DBFindQuery(JavaPlugin owner, Collection col, DBObject query,
			DBObject keys, JobCallback callback) {
		this(owner, Core.get().getDatabaseManager().getCollection(col), query,
				keys, callback);
	}

	@SuppressWarnings({ "deprecation", "rawtypes" })
	@Override
	protected int doTaskStart(JavaPlugin owner, JobCallback callback) {
		return Bukkit.getScheduler().scheduleAsyncDelayedTask(owner, this);
	}

	public DBObject getResult() {
		return this.result;
	}

	@Override
	protected void abortTask() {
	}

	@Override
	protected boolean allowPause() {
		return false;
	}

	@Override
	protected void pauseTask() {
	}

	@Override
	protected void resumeTask() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		if (keys != null) {
			result = col.findOne(query, keys);
		} else {
			result = col.findOne(query);
		}
		getCallback(JobCallback.class).onDone(getTaskId(), this);
	}
}
