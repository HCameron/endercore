package com.enderage.core;

import org.bukkit.entity.Player;

/**
 * 
 * @author TwistPvP
 */
public abstract class OverridableTools {

	public abstract void kickPlayer(Player p, String reason);
}
