package com.enderage.core.perms;

import java.util.ArrayList;

import org.bson.types.ObjectId;
import org.bukkit.ChatColor;

import com.enderage.core.Core;
import com.enderage.core.database.DBLoadSync;

/**
 * 
 * @author TwistPvP
 * 
 */
public class PermissionGroup {

	private String alias;
	private String displayName;
	private ArrayList<String> permissions;
	private String parentAlias;
	private boolean isDefault;
	private ChatColor color;
	private ObjectId dbId;

	public PermissionGroup() {
		this.alias = "";
		this.displayName = "";
		this.permissions = new ArrayList<String>();
		this.isDefault = false;
		this.parentAlias = null;
		this.color = ChatColor.WHITE;
		this.dbId = null;
	}

	public boolean hasPermission(String perm) {
		return permissions.contains(perm);
	}

	public boolean changeAlias(String name) {
		if (alias.equals(name)) {
			return false;
		}
		alias = name;
		return true;
	}

	public boolean changeDisplayName(String name) {
		if (displayName.equals(name)) {
			return false;
		}
		displayName = name;
		return true;
	}

	public boolean addPermission(String node) {
		if (!permissions.contains(node)) {
			permissions.add(node);
			return true;
		}
		return false;
	}

	public boolean deletePermission(String node) {
		if (permissions.contains(node)) {
			permissions.remove(node);
			return true;
		}
		return false;
	}

	public String getDisplayName() {
		return displayName;
	}

	public ArrayList<String> getPermissions() {
		return permissions;
	}

	@DBLoadSync(columnName = "permissions")
	public void setPermissions(ArrayList<String> perms) {
		this.permissions = perms;
	}

	public String getAlias() {
		return alias;
	}

	@DBLoadSync(columnName = "displayName")
	public void setDisplayName(String newname) {
		displayName = newname;
	}

	@DBLoadSync(columnName = "alias")
	public void setAlias(String newalias) {
		alias = newalias;
	}

	@DBLoadSync(columnName = "defaultGroup")
	public void setDefault(boolean value) {
		this.isDefault = value;
	}

	@DBLoadSync(columnName = "parentAlias")
	public void setParentAlias(String alias) {
		this.parentAlias = alias;
	}

	@DBLoadSync(columnName = "color")
	public void setColor(String s) {
		this.color = ChatColor.valueOf(s);
	}

	public ChatColor getColor() {
		return this.color;
	}

	@DBLoadSync(columnName = "_id")
	public void setObjectId(Object obj) {
		this.dbId = (ObjectId) obj;
	}

	public ObjectId getObjectId() {
		return this.dbId;
	}

	public PermissionGroup getParent() {
		return Core.getPermManager().getByAlias(parentAlias);
	}

	public boolean isDefault() {
		return this.isDefault;
	}
}