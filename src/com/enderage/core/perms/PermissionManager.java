package com.enderage.core.perms;

import java.util.ArrayList;

import org.bukkit.ChatColor;

import com.enderage.core.Core;
import com.enderage.core.database.Collection;
import com.enderage.core.database.DBInstanceLoader;
import com.enderage.core.player.EnderPlayer;
import com.enderage.core.scheduler.callbacks.JobCallback;
import com.enderage.core.scheduler.main.Task;
import com.enderage.core.scheduler.tasks.BasicAsyncTask;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;

/**
 * 
 * @author TwistPvP
 * 
 */
public class PermissionManager {

	private PermissionGroup defaultGroup;
	private ArrayList<PermissionGroup> permGroups;

	public PermissionManager() {
		permGroups = new ArrayList<>();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<PermissionGroup> getPermissionGroups() {
		return (ArrayList<PermissionGroup>) permGroups.clone();
	}

	public void addPermissionGroup(PermissionGroup addition) {
		permGroups.add(addition);
	}

	public void delPermissionGroup(PermissionGroup deletion) {
		permGroups.remove(deletion);
	}

	public PermissionGroup getByAlias(String name) {
		PermissionGroup f = null;
		for (PermissionGroup p : permGroups) {
			if (p.getAlias().equalsIgnoreCase(name)) {
				f = p;
				break;
			}
		}
		return f;
	}

	public ArrayList<String> getNodesForGroup(final PermissionGroup[] group) {
		ArrayList<String> nodes = new ArrayList<String>();
		for (PermissionGroup pg : group) {
			PermissionGroup c = pg;
			nodes.addAll(c.getPermissions());
			while (c.getParent() != null) {
				nodes.addAll(c.getParent().getPermissions());
				c = c.getParent();
			}
		}
		return nodes;
	}

	public boolean hasPermission(EnderPlayer p, String node) {
		if (p.getPlayer().isOp()) {
			return true;
		}
		ArrayList<String> nodes = getNodesForGroup(p.getPermissions());
		if (nodes.contains("*")) {
			return true;
		}
		for (String n : nodes) {
			if (n.equals(node)) {
				return true;
			}
		}
		return false;
	}

	public void initialize(DBInstanceLoader ins) {
		permGroups.clear();
		permGroups.addAll(ins.initializeAndGet(Collection.RANKS,
				PermissionGroup.class));
		Core.get().Log(ChatColor.GREEN,
				"Loaded " + permGroups.size() + " ranks.");
		for (PermissionGroup g : permGroups) {
			if (g.isDefault()) {
				this.defaultGroup = g;
				return;
			}
		}
	}

	public PermissionGroup getDefaultPermission() {
		return this.defaultGroup;
	}

	public void saveAllGroups(boolean async) {
		for (PermissionGroup g : permGroups) {
			if (async) {
				saveGroupSync(g);
			} else {
				saveGroup(g);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	public void saveGroup(final PermissionGroup g) {
		Core.get().getTaskManager()
				.startTask(new BasicAsyncTask(Core.get(), new JobCallback() {

					@Override
					public void onDone(int taskId, Task obj) {
						Core.getPermManager().saveGroupSync(g);
					}

					@Override
					public void onAbort(int taskId, Task obj) {

					}

				}));
	}

	public void saveGroupSync(PermissionGroup g) {
		BasicDBObject obj = new BasicDBObject();

		BasicDBList perms = new BasicDBList();
		for (String s : g.getPermissions()) {
			perms.add(s);
		}

		obj.put("permissions", perms);
		obj.put("displayName", g.getDisplayName());
		obj.put("alias", g.getAlias());
		obj.put("defaultGroup", g.isDefault());
		obj.put("parentAlias", g.getParent().getAlias());
		obj.put("color", g.getColor().toString());

		DBCollection col = Core.get().getDatabaseManager()
				.getCollection(Collection.RANKS);

		if (g.getObjectId() != null) {
			col.update(new BasicDBObject("_id", g.getObjectId()),
					new BasicDBObject("$set", obj));
		} else {
			col.insert(obj);
			g.setObjectId(obj.get("_id"));
		}
	}
}
