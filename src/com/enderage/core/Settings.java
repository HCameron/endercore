package com.enderage.core;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;

import com.enderage.core.database.Collection;
import com.enderage.core.database.DBInstanceLoader;
import com.enderage.core.database.instance.Setting;

/**
 * 
 * @author TwistPvP
 */
public class Settings {

	private Setting[] settings;

	public Settings() {
		settings = new Setting[0];
	}

	public void initialize(DBInstanceLoader ins) {
		settings = ins.initializeAndGet(Collection.SETTINGS, Setting.class)
				.toArray(new Setting[0]);
		Core.get().Log(ChatColor.GREEN,
				"Loaded " + settings.length + " settings");
	}

	public Setting get(String key) {
		for (Setting s : settings) {
			if (s.getName().equals(key)) {
				return s;
			}
		}
		return null;
	}

	public String getString(String key) {
		return get(key).asString();
	}

	public int getInt(String key) {
		return get(key).asNumber().intValue();
	}

	public long getLong(String key) {
		return get(key).asNumber().longValue();
	}

	public short getShort(String key) {
		return get(key).asNumber().shortValue();
	}

	public byte getByte(String key) {
		return get(key).asNumber().byteValue();
	}

	public float getFloat(String key) {
		return get(key).asNumber().floatValue();
	}

	public double getDouble(String key) {
		return get(key).asNumber().doubleValue();
	}

	public Location getLocation(World w, String key) {
		return get(key).asLocation(w);
	}

	public List<?> getList(String key) {
		return get(key).asList();
	}

	public Number getNumber(String key) {
		return get(key).asNumber();
	}

	public List<Number> getNumberList(String key) {
		return get(key).asNumberList();
	}

	public List<String> getStringList(String key) {
		return get(key).asStringList();
	}

	public String[] getStringArray(String key) {
		return get(key).asStringArray();
	}
}
