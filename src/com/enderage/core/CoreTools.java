package com.enderage.core;

import org.bukkit.entity.Player;

/**
 * 
 * @author TwistPvP
 */
public class CoreTools extends OverridableTools {

	@Override
	public void kickPlayer(Player p, String reason) {
		p.kickPlayer(reason);
	}
}
