package com.enderage.core.utils;

/**
 * 
 * @author TwistPvP
 * 
 */
public class BooleanUtils {

	public static String boolToString(boolean bool) {
		if (bool) {
			return "true";
		}
		return "false";
	}

	public static boolean stringToBool(String bool) {
		if (bool.equals("true")) {
			return true;
		}
		return false;
	}
}
