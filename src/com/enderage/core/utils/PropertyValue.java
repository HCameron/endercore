package com.enderage.core.utils;

import java.util.ArrayList;
import java.util.Arrays;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

/**
 * 
 * @author TwistPvP
 */
public class PropertyValue {

	private String value;

	public PropertyValue(Object obj) {
		this.value = obj.toString();
	}

	public PropertyValue(JSONObject json) {
		this.value = json.toJSONString();
	}

	public PropertyValue(String s) {
		this.value = s;
	}

	@SuppressWarnings("unchecked")
	public PropertyValue(ArrayList<String> list) {
		JSONObject obj = new JSONObject();
		JSONArray array = new JSONArray();
		array.addAll(list);
		obj.put("array", value);
		this.value = obj.toJSONString();
	}

	public String getString() {
		return this.value;
	}

	public JSONObject getJSON() throws Exception {
		return (JSONObject) new JSONParser().parse(this.value);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<String> getStringArray() throws Exception {
		JSONObject obj = (JSONObject) new JSONParser().parse(this.value);
		JSONArray a = (JSONArray) obj.get("array");
		return (ArrayList<String>) Arrays.asList((String[]) a
				.toArray(new String[0]));
	}

	public int getInt() throws Exception {
		return Integer.parseInt(this.value);
	}

	public double getDouble() throws Exception {
		return Double.parseDouble(this.value);
	}

	@Override
	public String toString() {
		return getString();
	}
}
