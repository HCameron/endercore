package com.enderage.core.utils;

import java.io.File;
import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.ChatColor;

import com.enderage.core.Core;

/**
 * 
 * @author TwistPvP
 */
public class PropertiesFileManager {

	private HashMap<String, PropertiesFile> propertyFileMap;

	public PropertiesFileManager() {
		propertyFileMap = new HashMap<String, PropertiesFile>();
	}

	public PropertiesFile getPropertiesFile(String file) {
		if (this.propertyFileMap.containsKey(file)) {
			return this.propertyFileMap.get(file);
		}
		PropertiesFile pf = new PropertiesFile(new File(file));
		propertyFileMap.put(file, pf);
		return pf;
	}

	public void SaveAll() {
		for (Entry<String, PropertiesFile> e : propertyFileMap.entrySet()) {
			try {
				e.getValue().save();
				Core.get().Log(ChatColor.GREEN,
						"Saved config file: " + e.getKey());
			} catch (Exception ex) {
				Core.get().Log(ChatColor.RED,
						"Failed saving config file: " + e.getKey());
				ex.printStackTrace();
			}
		}
	}
}
