package com.enderage.core.utils;

/**
 * 
 * @author TwistPvP
 * 
 * @param <T>
 */
public class ModifiableFinal<T> {

	private T value;

	public ModifiableFinal() {
	}

	public ModifiableFinal(T initialValue) {
		this.value = initialValue;
	}

	public void setValue(T newValue) {
		this.value = newValue;
	}

	public T getValue() {
		return this.value;
	}
}