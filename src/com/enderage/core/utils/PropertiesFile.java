package com.enderage.core.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Map.Entry;

/**
 * 
 * @author TwistPvP
 */
public class PropertiesFile extends PropertyMap {

	private File theFile;

	private ArrayList<String> descriptionLines;
	private boolean loaded;
	private boolean reading;

	public PropertiesFile(File f) {
		this.theFile = f;
		this.descriptionLines = new ArrayList<String>();
		this.loaded = false;
		this.reading = false;
	}

	public void load() throws Exception {
		if (this.reading) {
			throw new Exception("Already reading the properties: "
					+ theFile.getPath());
		}
		this.reading = true;
		if (!theFile.exists()) {
			try {
				theFile.createNewFile();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		BufferedReader r = new BufferedReader(new FileReader(theFile));
		String line;
		if (this.loaded) {
			this.clear();
		}
		boolean descr = true;
		while ((line = r.readLine()) != null) {
			if (!line.startsWith("#") && line.contains("=")) {
				descr = false;
				String key = line.split("=")[0].trim();
				String value = line.substring(line.indexOf("=") + 1).trim();
				this.setProperty(key, new PropertyValue(value));
			} else if (descr) {
				if (line.trim().length() > 0) {
					descriptionLines.add(line);
				}
			}
		}
		r.close();
		this.loaded = true;
		this.reading = false;
	}

	public void save() throws Exception {
		BufferedWriter bw = new BufferedWriter(new FileWriter(theFile, false));
		for (String s : descriptionLines) {
			bw.write(s);
			bw.newLine();
		}
		bw.newLine();
		for (Entry<String, PropertyValue> e : this.getEntrySet()) {
			bw.write(e.getKey() + " = " + e.getValue().toString());
			bw.newLine();
		}
		bw.close();
	}
}
