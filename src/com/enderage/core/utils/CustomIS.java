package com.enderage.core.utils;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * 
 * @author TwistPvP
 * 
 */
public class CustomIS {

	private String name = "";
	private List<String> lore = new ArrayList<>();
	private Material material = Material.DIAMOND;
	private byte data = 0;
	private int size = 1;

	public CustomIS setName(String name) {
		this.name = name;
		return this;
	}

	public CustomIS addLore(String line) {
		lore.add(line);
		return this;
	}

	public CustomIS setMaterial(Material mat) {
		material = mat;
		return this;
	}

	public CustomIS setData(byte data) {
		this.data = data;
		return this;
	}

	public CustomIS setSize(int size) {
		this.size = size;
		return this;
	}

	public ItemStack get() {
		ItemStack is = new ItemStack(material, data);
		is.setAmount(size);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(name);
		im.setLore(lore);
		is.setItemMeta(im);
		return is;
	}
}
