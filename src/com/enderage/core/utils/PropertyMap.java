package com.enderage.core.utils;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 
 * @author TwistPvP
 */
public class PropertyMap {

	private HashMap<String, PropertyValue> settings;

	public PropertyMap() {
		this.settings = new HashMap<String, PropertyValue>();
	}

	public PropertyValue getProperty(String name) {
		return this.settings.get(name);
	}

	public PropertyValue getProperty(String name, PropertyValue fallback) {
		PropertyValue pv = getProperty(name);
		if (pv != null) {
			return pv;
		}
		settings.put(name, fallback);
		return fallback;
	}

	public boolean hasProperty(String name) {
		return this.settings.containsKey(name);
	}

	public void remove(String name) {
		this.settings.remove(name);
	}

	public void setProperty(String name, PropertyValue value) {
		if (this.hasProperty(name)) {
			this.remove(name);
		}
		this.settings.put(name, value);
	}

	public void clear() {
		this.settings.clear();
	}

	public Set<Entry<String, PropertyValue>> getEntrySet() {
		return this.settings.entrySet();
	}
}
