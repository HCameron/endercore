package com.enderage.core.utils;

import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * 
 * @author TwistPvP
 */
public class PlayerUtils {

	public static String getStringUuid(Player p) {
		return getStringUuid(p.getUniqueId());
	}

	public static UUID getBukkitUuid(String uuid) {
		return UUID.fromString(uuid.substring(0, 8) + "-"
				+ uuid.substring(8, 12) + "-" + uuid.substring(12, 16) + "-"
				+ uuid.substring(16, 20) + "-" + uuid.substring(20, 32));
	}

	public static String getStringUuid(UUID uuid) {
		return uuid.toString().replaceAll("-", "");
	}

	public static Player getByUUID(String uuid) {
		Player f = null;
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (getStringUuid(p).equalsIgnoreCase(uuid)) {
				f = p;
				break;
			}
		}
		return f;
	}

	public static Player getByUUID(UUID uuid) {
		return getByUUID(getStringUuid(uuid));
	}
}
