package com.enderage.core.utils;

import java.util.Arrays;
import java.util.List;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * 
 * @author TwistPvP
 * 
 */
public class ItemUtils {

	public static ItemStack setLore(ItemStack is, List<String> lore) {
		ItemMeta im = is.getItemMeta();
		im.setLore(lore);
		is.setItemMeta(im);
		return is;
	}

	public static ItemStack setName(ItemStack is, String name) {
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(name);
		is.setItemMeta(im);
		return is;
	}

	public static ItemStack setLore(ItemStack is, String[] lore) {
		return setLore(is, Arrays.asList(lore));
	}
}
