package com.enderage.core.cmd.commands;

import java.util.regex.Pattern;

import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import com.enderage.core.Core;
import com.enderage.core.cmd.CMDExecutor;
import com.enderage.core.cmd.EnderCommand;
import com.enderage.core.player.EnderPlayer;

/**
 * 
 * @author TwistPvP
 */
public class nick extends EnderCommand {

	public nick(String name) {
		super(name);
	}

	@Override
	public String getUsage() {
		return "<nick/off>";
	}

	@Override
	public String getDescription() {
		return "Sets your nickname or disables it";
	}

	@Override
	public CMDExecutor allowedExecutor() {
		return CMDExecutor.PLAYER;
	}

	@Override
	public void runPlayer(Player p, EnderPlayer ep, Command cmd, String[] args) {
		String nick = args[0];
		if (nick.equalsIgnoreCase("off")) {
			ep.setNick(ep.getUsername());
			Core.getPlayerManager().setTablistColor(ep);
			success(p, "Reset your nickname!");
		} else {
			if (nick.length() >= 3
					&& nick.length() <= 15
					&& Pattern.compile("^[a-z0-9_-]{3,15}$")
							.matcher(nick.toLowerCase()).matches()) {
				ep.setNick(nick);
				Core.getPlayerManager().setTablistColor(ep);
				success(p, "Set your nick to " + nick);
			} else {
				error(p, "Invalid nickname! Needs to be 3-15 characters!");
			}
		}
	}

	@Override
	public void runConsole(ConsoleCommandSender cs, Command cmd, String[] args) {
	}

	@Override
	public void runBlock(BlockCommandSender cs, Command cmd, String[] args) {
	}

	@Override
	public void runAll(CommandSender cs, Command cmd, String[] args) {
	}
}
