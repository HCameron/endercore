package com.enderage.core.cmd.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import com.enderage.core.Core;
import com.enderage.core.cmd.CMDExecutor;
import com.enderage.core.cmd.EnderCommand;
import com.enderage.core.player.EnderPlayer;
import com.enderage.core.scheduler.callbacks.JobCallback;
import com.enderage.core.scheduler.main.Task;

/**
 * 
 * @author TwistPvP
 */
public class debugcore extends EnderCommand {

	public debugcore(String name) {
		super(name);
	}

	@Override
	public String getUsage() {
		return "<option> [args]";
	}

	@Override
	public String getDescription() {
		return "Debugging tools for developers";
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void runPlayer(final Player p, EnderPlayer ep, Command cmd,
			String[] args) {
		try {
			if (args.length > 0) {
				switch (args[0].toLowerCase()) {
				case "playermanager": {
					Core.get();
					success(p, "Player manager size: "
							+ Core.getPlayerManager().getAll().length);
					success(p, "Player online count: "
							+ Bukkit.getOnlinePlayers().size());
					break;
				}
				case "altermaxplayers": {
					int max = Integer.parseInt(args[1]);
					// Field f =
					// PlayerList.class.getDeclaredField("maxPlayers");
					// f.setAccessible(true);
					// f.setInt(((CraftServer) Bukkit.getServer()).getHandle(),
					// max);
					success(p, "Max players set to: " + max);
					break;
				}
				case "relog": {
					@SuppressWarnings("deprecation")
					final Player w = Bukkit.getPlayer(args[1]);
					if (w != null) {
						Core.getPlayerManager().reloadPlayer(p,
								new JobCallback() {
									@Override
									public void onDone(int taskId, Task obj) {
										success(p,
												"Succesfully reloaded player: "
														+ w.getName());
									}

									@Override
									public void onAbort(int taskId, Task obj) {
										error(p, "Failed reloading player: "
												+ w.getName());
									}
								});
					}
					break;
				}
				default: {
					sendUsage(p);
					break;
				}
				}
			} else {
				sendUsage(p);
			}
		} catch (Exception ex) {
			System.out.println("/debugcore: " + ex.getMessage());
			error(p, "Something went wrong :S");
			sendUsage(p);
		}
	}

	@Override
	public CMDExecutor allowedExecutor() {
		return CMDExecutor.PLAYER;
	}

	@Override
	public void runConsole(ConsoleCommandSender cs, Command cmd, String[] args) {
	}

	@Override
	public void runBlock(BlockCommandSender cs, Command cmd, String[] args) {
	}

	@Override
	public void runAll(CommandSender cs, Command cmd, String[] args) {
	}
}
