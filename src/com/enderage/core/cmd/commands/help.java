package com.enderage.core.cmd.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import com.enderage.core.Core;
import com.enderage.core.cmd.CMDExecutor;
import com.enderage.core.cmd.EnderCommand;
import com.enderage.core.cmd.LoadedCommand;
import com.enderage.core.player.EnderPlayer;

/**
 * 
 * @author TwistPvP
 */
public class help extends EnderCommand {

	public help(String name) {
		super(name);
	}

	@Override
	public String getUsage() {
		return "[page/command]";
	}

	@Override
	public String getDescription() {
		return "List commands or show specific command info";
	}

	@Override
	public CMDExecutor allowedExecutor() {
		return CMDExecutor.ALL;
	}

	@Override
	public void runPlayer(Player p, EnderPlayer ep, Command cmd, String[] args) {
	}

	@Override
	public void runConsole(ConsoleCommandSender cs, Command cmd, String[] args) {
	}

	@Override
	public void runBlock(BlockCommandSender cs, Command cmd, String[] args) {
	}

	@Override
	public void runAll(CommandSender cs, Command cmd, String[] args) {
		int page = 1;
		if (args.length > 0) {
			try {
				page = Integer.parseInt(args[0]);
			} catch (Exception ex) {
				showCommandHelp(cs, cmd, args);
				return;
			}
		}
		ArrayList<EnderCommand> allowedCommands = getAllowedCommands(cs);
		int cmdsPerPage = 9;
		int totalPages = ((allowedCommands.size() / cmdsPerPage) + (((allowedCommands
				.size() % cmdsPerPage) != 0) ? 1 : 0));

		if (page < 1) {
			page = 1;
		}

		int start = (page * cmdsPerPage) - cmdsPerPage;
		int end = (page * cmdsPerPage);

		if (start > allowedCommands.size()) {
			start = totalPages - 1;
			end = totalPages + cmdsPerPage - 1;
		}
		if (end > allowedCommands.size()) {
			end = allowedCommands.size() - 1;
		}
		if (start < 0) {
			error(cs, "Wrong index...");
			return;
		}

		List<EnderCommand> RallowedCommands = allowedCommands.subList(start,
				end);

		if (page > totalPages) {
			page = totalPages;
		}

		cs.sendMessage(ChatColor.DARK_GRAY + "  [========== "
				+ ChatColor.DARK_AQUA + "Help (Page " + page + "/" + totalPages
				+ ") " + ChatColor.DARK_GRAY + "==========]");
		for (EnderCommand scmd : RallowedCommands) {
			cs.sendMessage(ChatColor.GRAY + "/" + scmd.getName() + " - "
					+ ChatColor.WHITE + scmd.getDescription());
		}
	}

	private ArrayList<EnderCommand> getAllowedCommands(CommandSender cs) {
		List<LoadedCommand> all = Core.get().getCommandLoader()
				.getAllRegisteredCommands();
		ArrayList<EnderCommand> allowedCommands = new ArrayList<EnderCommand>();
		EnderPlayer check = null;
		if (cs instanceof Player) {
			check = Core.getPlayerManager().getEnderPlayer((Player) cs);
		}
		for (LoadedCommand c : all) {
			if (check == null) {
				allowedCommands.add(c.getCommand());
			} else {
				if (Core.getPermManager().hasPermission(check,
						c.getCommand().getPermission())) {
					allowedCommands.add(c.getCommand());
				}
			}
		}
		return allowedCommands;
	}

	private void showCommandHelp(CommandSender cs, Command cmd, String[] args) {
		ArrayList<EnderCommand> allowedCommands = getAllowedCommands(cs);
		EnderCommand c = null;
		for (EnderCommand l : allowedCommands) {
			if (l.getName().equalsIgnoreCase(args[0])) {
				c = l;
				break;
			}
			if (l.getAliases() != null) {
				for (String s : l.getAliases()) {
					if (s.equalsIgnoreCase(args[0])) {
						c = l;
						break;
					}
				}
			}
		}
		if (c == null) {
			error(cs, "Command not found!");
		}

		cs.sendMessage(ChatColor.DARK_GRAY + "  [========== "
				+ ChatColor.DARK_AQUA + c.getName() + " Command Help "
				+ ChatColor.DARK_GRAY + "==========]");

		cs.sendMessage(ChatColor.GRAY + "Usage: " + ChatColor.WHITE + "/"
				+ c.getName() + " " + c.getUsage());
		cs.sendMessage(ChatColor.GRAY + "Description: " + ChatColor.WHITE
				+ c.getDescription());
		cs.sendMessage(ChatColor.GRAY + "Permission node: " + ChatColor.WHITE
				+ c.getPermission());
	}
}
