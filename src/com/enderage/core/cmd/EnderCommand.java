package com.enderage.core.cmd;

import org.bukkit.ChatColor;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import com.enderage.core.Core;
import com.enderage.core.player.EnderPlayer;

/**
 * 
 * @author TwistPvP
 * 
 */
public abstract class EnderCommand implements CommandExecutor {

	private String name;

	public EnderCommand(String name) {
		this.name = name;
	}

	public String[] getAliases() {
		return new String[0];
	}

	public abstract String getUsage();

	public String getName() {
		return this.name;
	}

	public abstract String getDescription();

	public String getPermission() {
		return Core.get().PERMISSION_PREFIX + ".commands." + name;
	}

	public abstract CMDExecutor allowedExecutor();

	public abstract void runPlayer(Player p, EnderPlayer ep, Command cmd,
			String[] args);

	public abstract void runConsole(ConsoleCommandSender cs, Command cmd,
			String[] args);

	public abstract void runBlock(BlockCommandSender cs, Command cmd,
			String[] args);

	public abstract void runAll(CommandSender cs, Command cmd, String[] args);

	protected void error(CommandSender cs, String msg) {
		colored(cs, ChatColor.RED, msg);
	}

	protected void success(CommandSender cs, String msg) {
		colored(cs, ChatColor.GREEN, msg);
	}

	protected void normal(CommandSender cs, String msg) {
		colored(cs, ChatColor.GRAY, msg);
	}

	protected void colored(CommandSender cs, ChatColor c, String msg) {
		cs.sendMessage(Core.CORE_PREFIX + c + msg);
	}

	protected void sendUsage(CommandSender cs) {
		normal(cs, "Usage: /" + getName() + " " + getUsage());
	}

	public String[] help() {
		return new String[] {
				ChatColor.GOLD + "/" + getName() + " " + getUsage(),
				ChatColor.YELLOW + getDescription() };
	}

	@SuppressWarnings("static-access")
	@Override
	public boolean onCommand(CommandSender cs, Command cmnd, String string,
			String[] args) {
		try {
			if (cs instanceof ConsoleCommandSender) {
				if (!allowedExecutor().equals(CMDExecutor.ALL)
						&& !allowedExecutor().equals(CMDExecutor.CONSOLE)) {
					error(cs, "This command cannot be ran by the console!");
					return true;
				}
				runConsole((ConsoleCommandSender) cs, cmnd, args);
				if (allowedExecutor().equals(CMDExecutor.ALL)) {
					runAll(cs, cmnd, args);
				}
				return true;
			}
			if (cs instanceof BlockCommandSender) {
				if (!allowedExecutor().equals(CMDExecutor.ALL)
						&& !allowedExecutor().equals(CMDExecutor.COMMANDBLOCK)) {
					error(cs, "This command cannot be ran by a command block!");
					return true;
				}
				runBlock((BlockCommandSender) cs, cmnd, args);
				if (allowedExecutor().equals(CMDExecutor.ALL)) {
					runAll(cs, cmnd, args);
				}
				return true;
			}
			if (!allowedExecutor().equals(CMDExecutor.ALL)
					&& !allowedExecutor().equals(CMDExecutor.PLAYER)) {
				error(cs, "This command cannot be ran by a player!");
				return true;
			}
			if (getPermission() != null) {
				if (!Core.getPermManager().hasPermission(
						Core.get().getPlayerManager()
								.getEnderPlayer((Player) cs), getPermission())) {
					error(cs,
							"You don't have the permission to execute this command!");
					return true;
				}
			}
			runPlayer((Player) cs, Core.get().getPlayerManager()
					.getEnderPlayer((Player) cs), cmnd, args);
			if (allowedExecutor().equals(CMDExecutor.ALL)) {
				runAll(cs, cmnd, args);
			}
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}
	//
	// private void applyCommandBlockOutput(BlockCommandSender cs, int i) {
	// try {
	// int x = cs.getBlock().getLocation().getBlockX();
	// int y = cs.getBlock().getLocation().getBlockY();
	// int z = cs.getBlock().getLocation().getBlockZ();
	// TileEntity t = ((CraftWorld) cs.getBlock().getWorld()).getTileEntityAt(x,
	// y, z);
	// if (t != null && t instanceof TileEntityCommand) {
	// TileEntityCommand te = (TileEntityCommand)t;
	// Field f = CommandBlockListenerAbstract.class.getDeclaredField("b");
	// f.setAccessible(true);
	// f.setInt(te.a(), i); // set output value
	//
	// World w = ((CraftWorld) cs.getBlock().getWorld()).getHandle();
	// w.f(x, y, z, w.getType(x, y, z)); // update comparators
	// System.out.println("Commandblock updated: " + te.a().g());
	// } else {
	// throw new Exception ("Block wasn't a commandblock or null!");
	// }
	//
	// } catch (Exception ex) {
	// ex.printStackTrace();
	// }
	// }
}
