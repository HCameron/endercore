package com.enderage.core.cmd;

import java.io.FileInputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

import com.enderage.core.Core;
import com.enderage.core.CorePlugin;

/**
 * 
 * @author TwistPvP
 */
public class CommandLoader {

	private ConcurrentMap<String, LoadedCommand> loadedCommands = new ConcurrentHashMap<>();

	public List<LoadedCommand> getCommandsForPlugin(JavaPlugin plugin) {
		List<LoadedCommand> cmds = new ArrayList<LoadedCommand>();
		for (LoadedCommand c : loadedCommands.values()) {
			if (c.getOwner() == plugin) {
				cmds.add(c);
			}
		}
		return cmds;
	}

	public LoadedCommand getCommand(String name) {
		return loadedCommands.get(name);
	}

	public LoadedCommand findCommand(String nameOrAlias) {
		LoadedCommand f = getCommand(nameOrAlias);
		if (f != null) {
			return f;
		}
		boolean found = false;
		for (LoadedCommand lc : loadedCommands.values()) {
			for (String s : lc.getCommand().getAliases()) {
				if (s.equalsIgnoreCase(nameOrAlias)) {
					f = lc;
					found = true;
					break;
				}
			}
			if (lc.getCommand().getName().equalsIgnoreCase(nameOrAlias)) {
				f = lc;
				break;
			}
			if (found) {
				break;
			}
		}
		return f;
	}

	public List<LoadedCommand> getAllRegisteredCommands() {
		return new ArrayList<LoadedCommand>(loadedCommands.values());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void loadPluginCommands(CorePlugin plugin, String artifactId,
			String packageName) {
		String p = "plugins/" + artifactId + ".jar";
		List<String> l = getClassNamesInPackage(p, packageName);
		int i = 0;
		for (String cmd : l) {
			try {
				String cmdName = cmd.substring(cmd.lastIndexOf(".") + 1)
						.toLowerCase();
				Class c = plugin.getClass().getClassLoader().loadClass(cmd);
				if (EnderCommand.class.isAssignableFrom(c)) {
					Constructor<EnderCommand> commandExecutorConstructor = c
							.getDeclaredConstructor(String.class);
					EnderCommand cmdExec = (EnderCommand) commandExecutorConstructor
							.newInstance(cmdName);
					if (loadedCommands.containsKey(cmdName)) {
						throw new Exception("Command already registered: "
								+ cmd);
					}
					loadedCommands.put(cmdName, new LoadedCommand(plugin,
							cmdExec));
					if (registerBukkitCommand(plugin, cmdExec)) {
						plugin.getCommand(cmdExec.getName()).setExecutor(
								cmdExec);
						i++;
					} else {
						throw new Exception(
								"Failed to register command in bukkit: " + cmd);
					}
				} else {
					plugin.Log(
							ChatColor.RED,
							"class "
									+ cmd
									+ " isn't a EnderCommand! Please put all commands in a seperate command-only package!");
				}
			} catch (Exception ex) {
				System.out.println("Failed loading command: " + cmd);
				ex.printStackTrace();
			}
		}
		ChatColor cc = ChatColor.GREEN;
		if (i < l.size()) {
			cc = ChatColor.RED;
		}
		plugin.Log(cc, "Loaded " + i + "/" + l.size() + " commands.");
	}

	@SuppressWarnings("resource")
	private List<String> getClassNamesInPackage(String jarName,
			String packageName) {
		ArrayList<String> classes = new ArrayList<String>();

		packageName = packageName.replaceAll("\\.", "/");
		try {
			JarInputStream jarFile = new JarInputStream(new FileInputStream(
					jarName));
			JarEntry jarEntry;

			while (true) {
				jarEntry = jarFile.getNextJarEntry();
				if (jarEntry == null) {
					break;
				}
				if ((jarEntry.getName().startsWith(packageName))
						&& (jarEntry.getName().endsWith(".class"))) {
					String s = jarEntry.getName().replaceAll("/", "\\.");
					s = s.substring(0, s.lastIndexOf("."));
					if (!s.contains("$")) {
						classes.add(s);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return classes;
	}

	// I lost the craft server path and can't find it now -.- will download
	// later.
	/*
	 * private boolean registerBukkitCommand(JavaPlugin plugin, EnderCommand
	 * cmdExec) { try { Constructor<PluginCommand> cons = PluginCommand.class
	 * .getDeclaredConstructor(String.class, org.bukkit.plugin.Plugin.class);
	 * cons.setAccessible(true); PluginCommand cmd = (PluginCommand)
	 * cons.newInstance( cmdExec.getName(), plugin); ((CraftServer)
	 * plugin.getServer()).getCommandMap().register( cmdExec.getName(),
	 * Core.get().COMMAND_PREFIX, cmd); return true; } catch
	 * (NoSuchMethodException ex) {
	 * Logger.getLogger(CommandLoader.class.getName()).log(Level.SEVERE, null,
	 * ex); } catch (SecurityException ex) {
	 * Logger.getLogger(CommandLoader.class.getName()).log(Level.SEVERE, null,
	 * ex); } catch (InstantiationException ex) {
	 * Logger.getLogger(CommandLoader.class.getName()).log(Level.SEVERE, null,
	 * ex); } catch (IllegalAccessException ex) {
	 * Logger.getLogger(CommandLoader.class.getName()).log(Level.SEVERE, null,
	 * ex); } catch (IllegalArgumentException ex) {
	 * Logger.getLogger(CommandLoader.class.getName()).log(Level.SEVERE, null,
	 * ex); } catch (InvocationTargetException ex) {
	 * Logger.getLogger(CommandLoader.class.getName()).log(Level.SEVERE, null,
	 * ex); } catch (Exception ex) {
	 * Logger.getLogger(CommandLoader.class.getName()).log(Level.SEVERE, null,
	 * ex); } return false; }
	 */
}
