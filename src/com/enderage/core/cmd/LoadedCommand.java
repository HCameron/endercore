package com.enderage.core.cmd;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * 
 * @author TwistPvP
 */
public class LoadedCommand {

	private EnderCommand cmd;
	private JavaPlugin owner;

	public LoadedCommand(JavaPlugin plugin, EnderCommand cmd) {
		this.cmd = cmd;
		this.owner = plugin;
	}

	public EnderCommand getCommand() {
		return cmd;
	}

	public JavaPlugin getOwner() {
		return owner;
	}
}
