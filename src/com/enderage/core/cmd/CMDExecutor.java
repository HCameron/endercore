package com.enderage.core.cmd;

/**
 * 
 * @author TwistPvP
 */
public enum CMDExecutor {
	ALL, COMMANDBLOCK, CONSOLE, PLAYER;
}
