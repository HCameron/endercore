package com.enderage.core.database;

/**
 * 
 * @author TwistPvP
 */
public enum Collection {

	SETTINGS("Settings"), PLAYERS("Players"), RANKS("Ranks"), PERMISSIONS(
			"Permissions");

	private String dbName;

	private Collection(String name) {
		this.dbName = name;
	}

	@Override
	public String toString() {
		return dbName;
	}
}
