package com.enderage.core.database.instance;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.bukkit.Location;
import org.bukkit.World;

import com.enderage.core.database.DBLoadSync;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * 
 * @author TwistPvP
 */
public class Setting {

	private ObjectId ID;
	private String name;
	private Object value;

	public Setting() {
		// ensure null
		this.ID = null;
		this.name = null;
		this.value = null;
	}

	public Setting(String name, Object value) {
		this();
		this.name = name;
		if (value instanceof Location) {
			this.setLocation((Location) value);
			return;
		}
		this.value = value;
	}

	public ObjectId getID() {
		return ID;
	}

	@DBLoadSync(columnName = "_id")
	public void setID(ObjectId ID) {
		this.ID = ID;
	}

	public String getName() {
		return name;
	}

	@DBLoadSync(columnName = "key")
	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		return value;
	}

	public Number asNumber() {
		return (Number) value;
	}

	public String asString() {
		return (String) value;
	}

	public String[] asStringArray() {
		return ((BasicDBList) value).toArray(new String[0]);
	}

	// use generics to fix brackets at some point.

	@SuppressWarnings("rawtypes")
	public List asList() {
		return (List) value;
	}

	@SuppressWarnings("unchecked")
	public List<String> asStringList() {
		return (List<String>) value;
	}

	@SuppressWarnings("unchecked")
	public List<Number> asNumberList() {
		return (List<Number>) value;
	}

	public Location asLocation(World w) {
		List<Number> list = asNumberList();
		return new Location(w, list.get(0).doubleValue(), list.get(1)
				.doubleValue(), list.get(2).doubleValue(), list.get(3)
				.floatValue(), list.get(4).floatValue());
	}

	@DBLoadSync(columnName = "value")
	public void setValue(Object value) {
		this.value = value;
	}

	public void setLocation(Location location) {
		ArrayList<Number> list = new ArrayList<Number>();
		list.add(location.getX());
		list.add(location.getY());
		list.add(location.getZ());
		list.add(location.getYaw());
		list.add(location.getPitch());
		value = list;
	}

	public DBObject DBserialize() {
		BasicDBObject obj = new BasicDBObject();
		obj.put("key", getName());
		obj.put("value", getValue());
		return obj;
	}
}
