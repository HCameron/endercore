package com.enderage.core.database;

import org.bukkit.ChatColor;

import com.enderage.core.Core;
import com.enderage.core.utils.PropertyMap;
import com.enderage.core.utils.PropertyValue;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;

/**
 * 
 * @author TwistPvP
 */
public class DatabaseManager {

	private String dbUrl;
	@SuppressWarnings("unused")
	private String dbPass;
	private String dbDatabase;
	private int dbPort;

	private MongoClient client;

	public DatabaseManager() {

	}

	public void initialize(PropertyMap properties) {
		try {
			this.dbUrl = properties.getProperty("db-url",
					new PropertyValue("localhost")).getString();
			this.dbPass = properties.getProperty("db-pass",
					new PropertyValue("")).getString();
			this.dbDatabase = properties.getProperty("db-database",
					new PropertyValue("EnderCore")).getString();
			this.dbPort = properties.getProperty("db-port",
					new PropertyValue(27017)).getInt();

			client = new MongoClient(dbUrl, dbPort);
		} catch (Exception ex) {
			Core.get().Log(ChatColor.RED, "Failed connecting to the database!");
			ex.printStackTrace();
		}
	}

	public DB getDatabase(String name) {
		try {
			if (client == null) {
				Core.get()
						.Log(ChatColor.RED,
								"MongoClient not yet initialized! Cannot get database.");
				throw new Exception();
			}
			DB d = client.getDB(name);
			if (d == null) {
				Core.get().Log(ChatColor.RED,
						"Database " + name + " doesn't exist!");
				throw new Exception();
			}
			return d;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public DBCollection getDBCollection(String db, String col) {
		try {
			DB d = getDatabase(db);
			DBCollection c = d.getCollection(col);
			if (c != null) {
				return c;
			}
			Core.get().Log(
					ChatColor.RED,
					"Collection " + ChatColor.YELLOW + col + ChatColor.RED
							+ " in " + ChatColor.YELLOW + db + ChatColor.RED
							+ " doesn't exist!");
			throw new Exception();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public DBCollection getCollection(Collection col) {
		return getCollection(col.toString());
	}

	public DBCollection getCollection(String col) {
		DB d = getDatabase(dbDatabase);
		return d.getCollection(col);
	}

	public String getDefaultDB() {
		return this.dbDatabase;
	}
}
