package com.enderage.core.database;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @author TwistPvP
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface DBLoadSync {

	String columnName();

	@SuppressWarnings("rawtypes")
	Class referenceClass() default Object.class;
}
