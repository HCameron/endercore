package com.enderage.core.database;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import com.enderage.core.Core;
import com.enderage.core.callbacks.IDBInstanceLoadedCallback;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * 
 * @author TwistPvP
 */
public class DBInstanceLoader {

	private final String database;

	public DBInstanceLoader(String database) {
		this.database = database;
	}

	public <T> ArrayList<T> initializeAndGet(Collection col,
			Class<T> referenceClass) {
		return initializeAndGet(col, null, referenceClass);
	}

	public <T> ArrayList<T> initializeAndGet(Collection col, DBObject where,
			Class<T> referenceClass) {
		String table = col.toString();
		ArrayList<T> objects;

		DB db = Core.get().getDatabaseManager().getDatabase(database);

		if (where == null) {
			where = new BasicDBObject();
		}

		DBCursor result = db.getCollection(table).find(where);

		try {
			objects = new ArrayList<>();

			while (result.hasNext()) {
				DBObject next = result.next();
				T o = loadObject(next, referenceClass);
				if (o instanceof IDBInstanceLoadedCallback) {
					((IDBInstanceLoadedCallback) o).InstanceLoadedCallback();
				}
				objects.add(o);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		} finally {
			result.close();
		}

		return objects;
	}

	public <T> T loadObject(DBObject dbo, Class<T> referenceClass) {
		try {
			Method[] m = referenceClass.getDeclaredMethods();
			T o = referenceClass.newInstance();
			for (Method m1 : m) {
				DBLoadSync a = (DBLoadSync) m1.getAnnotation(DBLoadSync.class);
				if (a != null) {
					Object obj = null;
					try {
						if (dbo.containsField(a.columnName())) {
							Object object = dbo.get(a.columnName());
							obj = object;
							m1.invoke(o, new Object[] { obj });
						}
					} catch (InvocationTargetException ex) {
						if (obj != null) {
							System.out.println("Type: "
									+ obj.getClass().getName());
						} else {
							System.out.println("Type: NULL");
						}
						ex.printStackTrace();
					} catch (Exception ex) {
						ex.printStackTrace();
						System.out.println(m1.getName() + ": " + ex);
						System.out.println("Field Name: " + a.columnName());
						if (obj != null) {
							System.out.println("Type: "
									+ obj.getClass().getName());
						} else {
							System.out.println("Type: NULL");
						}
					}
				}
			}
			return o;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
